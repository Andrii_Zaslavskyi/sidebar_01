const expand_btn = document.querySelector(".expand-btn");

let activeIndex;

expand_btn.addEventListener("click", () => {
    document.body.classList.toogle("collapsed");
});

const current = window.location.href;

const allLinks = document.querySelectorAll(".sidebar-links a");

allLinks.forEach((elem) => {
    elem.addEventListener("click", function () {
        const hrefLinkKlick = elem.href;

        allLinks.forEach((link) => {
            if (link.href == hrefLinkKlick) {
                link.classList.add("active");
            } else {
                link.classList.remove("active");
            }
        });
    });
});